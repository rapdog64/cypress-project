/// <reference types="cypress" />

context('Assertions', () => {
  beforeEach(() => {
    cy.visit('https://example.cypress.io/commands/assertions')
  })

  describe('Implicit Assertions', () => {

    it('.should() - make an assertion about the current subject', () => {

      cy.allure().descriptionHtml('Make an assertion about the current subject')
        .testID('TS-21321321')
        
        .description('make an assertion about the current subject')
        .suite('My SUITE')
        .owner('Ivan')
        .AllureStep('Get Assertion-table');
        cy.get('.assertion-table');
        cy.AllureStep('Find the table');
        cy.find('tbody tr:last')
        .AllureStep('Check have.class success')
        .should('have.class', 'success')

        .AllureStep('find firt td')
        .find('td')
        .first()
        .AllureStep('Lots of assertions')
        .should('have.text', 'Column content')
        .should('contain', 'Column content')
        .should('have.html', 'Column content')
        // chai-jquery uses "is()" to check if element matches selector
        .should('match', 'td')
        // to match text content against a regular expression
        // first need to invoke jQuery method text()
        // and then match using regular expression
        .invoke('text')
        .should('match', /column content/i)

      // a better way to check element's text content against a regular expression
      // is to use "cy.contains"
      // https://on.cypress.io/contains
      cy.get('.assertion-table')
        .find('tbody tr:last')
        // finds first <td> element with text content matching regular expression
        .contains('td', /column content/i)
        .should('be.visible')

    })

    it('.and() - chain multiple assertions together2', () => {
    
      cy
        .allure()
        .owner('Ivan')
        .story('My story')
        .feature('MY FEATURE')
        .suite('My SUITE')
        .severity('critical')
        .step('loginssdasd')
        .tag('MY TAG')
        .get('.assertions-link')
        .allure().step('check that the class is active', true)
        .should('have.class', 'active')
        .allure().step('check that the class has href', true)
        .and('have.attr', 'href')
        .allure().step('check that the class include the link cypress.io', true)
        .and('include', 'cypress.io2')
    })
  })

  describe('Explicit Assertions', () => {
   
    it('expect - make an assertion about a specified subject', () => {
    
      expect(true).to.be.true
      const o = { foo: 'bar' }

      expect(o).to.equal(o)
      expect(o).to.deep.equal({ foo: 'bar' })
     
      expect('FooBar').to.match(/bar$/i)
    })

    it('pass your own callback function to should()', () => {
  
      cy.allure()
        .owner('Ivan')
        .story('My story')
        .feature('MY FEATURE')
        .suite('My SUITE')
        .severity('critical')
        .step('login')
        .tag('MY TAG')
        .severity('critical')
        .AllureStep('Allure 1')
        .get('.assertions-p')
        .AllureStep('Allure 2 find')
        .find('p')
        .AllureStep('Allure 3 check p')
        .should(($p) => {
         
          const texts = $p.map((i, el) => Cypress.$(el).text())
          const paragraphs = texts.get()
          expect(paragraphs, 'has 3 paragraphs').to.have.length(3)

  
          expect(paragraphs, 'has expected text in each paragraph').to.deep.eq([
            'Some text from first p',
            'More text from second p',
            'And even more text from third p2',
          ])
        })
    })

    it('finds element by class name regex', () => {

      cy.allure().suite('My SUITE');
      cy.get('.docs-header')
        .find('div')
        .should(($div) => {
          expect($div).to.have.length(1)

          const className = $div[0].className

          expect(className).to.match(/heading-/)
        })
        .then(($div) => {
          expect($div, 'text content').to.have.text('Introduction')
        })
    })

    it('can throw any error', () => {
      cy.get('.docs-header')
        .find('div')
        .should(($div) => {
          if ($div.length !== 1) {
          
            throw new Error('Did not find 1 element')
          }

          const className = $div[0].className

          if (!className.match(/heading-/)) {
            throw new Error(`Could not find class "heading-" in ${className}`)
          }
        })
    })

    it('matches unknown text between two elements', () => {

      cy.allure().suite('My SUITE');

      /**
       * Text from the first element.
       * @type {string}
      */
      let text

      /**
       * Normalizes passed text,
       * useful before comparing text with spaces and different capitalization.
       * @param {string} s Text to normalize
      */
      const normalizeText = (s) => s.replace(/\s/g, '').toLowerCase()

      cy.get('.two-elements')
        .find('.first')
        .then(($first) => {
          // save text from the first element
          text = normalizeText($first.text())
        })

      cy.get('.two-elements')
        .find('.second')
        .should(($div) => {
          // we can massage text before comparing
          const secondText = normalizeText($div.text())

          expect(secondText, 'second text').to.equal(text)
        })
    })

    it('assert - assert shape of an object', () => {
      const person = {
        name: 'Joe',
        age: 20,
      }

      assert.isObject(person, 'value is object')
    })

    it('retries the should callback until assertions pass', () => {
      cy.get('#random-number')
        .should(($div) => {
          const n = parseFloat($div.text())

          expect(n).to.be.gte(1).and.be.lte(10)
        })
    })
  })
})
